package com.kolyadruz.dabaanclient.Views.auth;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.kolyadruz.dabaanclient.Global;
import com.kolyadruz.dabaanclient.OnErrDialogCLickedListener;
import com.kolyadruz.dabaanclient.R;
import com.kolyadruz.dabaanclient.Retrofit.NetworkService;
import com.kolyadruz.dabaanclient.Retrofit.RequestBody;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.checklogin.ClientLoginData;
import com.kolyadruz.dabaanclient.Views.Settings.CityChooser;
import com.kolyadruz.dabaanclient.Views.orders.NewOrder;
import com.kolyadruz.dabaanclient.Views.orders.OrderSearchDriver;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kolyadruz on 31.03.2018.
 */

public class Login extends AppCompatActivity implements OnErrDialogCLickedListener {

    private static final String TAG = "FireBaseToken";

    String token;
    String fbtoken = "";

    Context context;
    OnErrDialogCLickedListener errDialodListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;
        errDialodListener = this;


        token = mSettings.getString("token", "");
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }

        // Get token
        String tkn = FirebaseInstanceId.getInstance().getToken();

        // Log and toast
        String msg = getString(R.string.msg_token_fmt, tkn);
        fbtoken = tkn;

        Log.d(TAG, msg);


        if (token.equals("")) {
            Log.d("Login:", "Токен не найден");
            Log.d("Login:", "Переход в Register.java");

            //Intent intent = new Intent(getApplicationContext(), Register.class);

            Intent intent = new Intent(getApplicationContext(), Chooser.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

            SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putString("fbtoken", fbtoken);
            editor.commit();

            startActivity(intent);

        } else {

            Log.d("Login:", "Токен найден "+token);
            Log.d("Login:", "Запуск проверки токена");

            checkLogin();
        }
    }

    private void checkLogin() {

        RequestBody body = new RequestBody();
        body.fb_token = fbtoken;
        body.token = token;

        NetworkService.getInstance()
                .getJSONApi()
                .clientlogin(body)
                .enqueue(new Callback<ClientLoginData>() {
                    @Override
                    public void onResponse(@NonNull Call<ClientLoginData> call, @NonNull Response<ClientLoginData> response) {

                        Gson gson = new Gson();

                        Log.d("Login", "response: " + gson.toJson(response.body()));

                        ClientLoginData responseData = response.body();

                        if (responseData.err_id == 0) {

                            Log.d("Login:","Токен рабочий");

                            Integer order_id = responseData.order.order_id;
                            Integer status = responseData.order.status;

                            SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
                            SharedPreferences.Editor editor = mSettings.edit();
                            editor.putString("cityName", responseData.city.name);
                            editor.putInt("cityId", responseData.city.id);
                            editor.putInt("minDiscountSum", responseData.min_discount_sum);
                            editor.putInt("balance", responseData.balance);
                            editor.putString("dispPhone", responseData.city.dispPhone);
                            editor.commit();

                            if (responseData.city.id == 0) {

                                Log.d("Login:", "Переход в CityChooser.java");

                                Intent intent = new Intent(getApplicationContext(), CityChooser.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                startActivity(intent);

                            } else {
                                if (status == 4 || status == 6 || status == 10 || status == 11) {
                                    Log.d("Login:","Нет активных заказов");
                                    Log.d("Login:","Переход в NewOrder.java");
                                    Intent intent = new Intent(getApplicationContext(), NewOrder.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                    startActivity(intent);
                                } else {
                                    Log.d("Login:","Есть активный заказ");
                                    Log.d("Login:","Переход в OrderSearchDriver.java");
                                    Intent intent = new Intent(getApplicationContext(), OrderSearchDriver.class);
                                    intent.putExtra("order_id", order_id);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                    startActivity(intent);
                                }
                            }
                        } else {
                            Global.showErrId(context, responseData.err_id, errDialodListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ClientLoginData> call, @NonNull Throwable t) {
                        //textView.append("Error occurred while getting request!");
                        Global.showErrorConnection(context, errDialodListener);
                        t.printStackTrace();
                    }
                });

    }

    @Override
    public void onOkButtonCLicked() {
        Intent intent = new Intent(getApplicationContext(), Register.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        startActivity(intent);
    }

    @Override
    public void onCancelButtonClicked() {

    }

    @Override
    public void onRetryButtonClicked() {
        checkLogin();
    }
}