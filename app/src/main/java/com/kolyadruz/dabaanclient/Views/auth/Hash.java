package com.kolyadruz.dabaanclient.Views.auth;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.kolyadruz.dabaanclient.Global;
import com.kolyadruz.dabaanclient.OnErrDialogCLickedListener;
import com.kolyadruz.dabaanclient.R;
import com.kolyadruz.dabaanclient.Retrofit.NetworkService;
import com.kolyadruz.dabaanclient.Retrofit.RequestBody;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.checkhash.CheckHashData;
import com.kolyadruz.dabaanclient.Views.Settings.CityChooser;
import com.kolyadruz.dabaanclient.Views.orders.NewOrder;
import com.kolyadruz.dabaanclient.Views.orders.OrderSearchDriver;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kolyadruz on 31.03.2018.
 */

public class Hash extends AppCompatActivity implements OnErrDialogCLickedListener {

    String phone;
    String fbtoken;

    TextView phoneNumTxt;
    EditText codeField;
    Button checkHashButton;

    Context context;
    OnErrDialogCLickedListener errDialogListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hash);

        context = this;
        errDialogListener = this;

        //String phoneNum = getIntent().getStringExtra("phone");

        phoneNumTxt = findViewById(R.id.textPlusSeven);

        codeField = findViewById(R.id.passField);
        checkHashButton = findViewById(R.id.checkHashButton);

        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
        phone = mSettings.getString("phone", "");

        phoneNumTxt.setText("+7" + phone);

        fbtoken = mSettings.getString("fbtoken", "");
    }

    public void checkCodeTapped(View view){
        view.setEnabled(false);
        checkFields();
        view.setEnabled(true);
    }

    private void checkFields() {
        if (codeField.length() == 4) {
            runCheckCode();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            String message = "Неверный код. Код должен быть не менее 4 символов.";
            builder.setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private void runCheckCode() {

        RequestBody body = new RequestBody();
        body.fb_token = fbtoken;
        body.phone = phone;
        body.hash = codeField.getText().toString();

        NetworkService.getInstance()
                .getJSONApi()
                .checkhash(body)
                .enqueue(new Callback<CheckHashData>() {
                    @Override
                    public void onResponse(@NonNull Call<CheckHashData> call, @NonNull Response<CheckHashData> response) {

                        Gson gson = new Gson();

                        Log.d("Hash", "response: " + gson.toJson(response.body()));

                        CheckHashData responseData = response.body();

                        if (responseData.err_id == 0) {

                            Integer order_id = responseData.order.order_id;
                            Integer status = responseData.order.status;

                            Log.d("Hash:","Код верный");

                            SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
                            SharedPreferences.Editor editor = mSettings.edit();
                            editor.putString("token", responseData.token);
                            editor.putInt("minDiscountSum", responseData.min_discount_sum);
                            editor.putInt("balance", responseData.balance);
                            editor.putString("dispPhone", responseData.city.dispPhone);
                            editor.commit();

                            if (responseData.city.id == 0) {

                                Log.d("Hash:", "Переход в CityChooser.java");

                                Intent intent = new Intent(getApplicationContext(), CityChooser.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                startActivity(intent);

                            } else {
                                if (status == 4 || status == 6 || status == 10 || status == 11) {
                                    Log.d("Hash:","Нет активных заказов");
                                    Log.d("Hash:","Переход в NewOrder.java");
                                    Intent intent = new Intent(getApplicationContext(), NewOrder.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                    startActivity(intent);
                                } else {
                                    Log.d("Hash:","Есть активный заказ");
                                    Log.d("Hash:","Переход в OrderSearchDriver.java");
                                    Intent intent = new Intent(getApplicationContext(), OrderSearchDriver.class);
                                    intent.putExtra("order_id", order_id);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                    startActivity(intent);
                                }
                            }
                        } else {
                            Global.showErrId(context, responseData.err_id, errDialogListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<CheckHashData> call, @NonNull Throwable t) {
                        //textView.append("Error occurred while getting request!");
                        Global.showErrorConnection(context, errDialogListener);
                        t.printStackTrace();
                    }
                });
    }

    @Override
    public void onOkButtonCLicked() {

    }

    @Override
    public void onCancelButtonClicked() {

    }

    @Override
    public void onRetryButtonClicked() {
        checkFields();
    }
}