package com.kolyadruz.dabaanclient.Views.orders;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.kolyadruz.dabaanclient.Global;
import com.kolyadruz.dabaanclient.R;

/**
 * Created by kolyadruz on 31.03.2018.
 */

public class OrderCompleted extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
        Integer theme = mSettings.getInt("theme", 0);
        switch (theme) {
            case 0:
                setTheme(R.style.StandartTheme);
                break;
            case 1:
                setTheme(R.style.PatriotTheme);
                break;
            case 2:
                setTheme(R.style.SilverTheme);
                break;
            default:
                break;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordercompleted);

        ConstraintLayout flag = findViewById(R.id.flag);

        if (theme == 1) {
            flag.setVisibility(View.VISIBLE);
        } else {
            flag.setVisibility(View.GONE);
        }

    }

    public void closeTapped(View view){
        view.setEnabled(false);

        Intent intent = new Intent(getApplicationContext(), NewOrder.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        startActivity(intent);

        view.setEnabled(true);
    }

}