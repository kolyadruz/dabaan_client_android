package com.kolyadruz.dabaanclient.Views.Settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.kolyadruz.dabaanclient.Global;
import com.kolyadruz.dabaanclient.OnErrDialogCLickedListener;
import com.kolyadruz.dabaanclient.R;
import com.kolyadruz.dabaanclient.Retrofit.NetworkService;
import com.kolyadruz.dabaanclient.Retrofit.RequestBody;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.getcities.CityDetail;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.getcities.GetCitiesData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.setcity.SetCityData;
import com.kolyadruz.dabaanclient.Views.orders.NewOrder;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CityChooser extends AppCompatActivity implements OnErrDialogCLickedListener {

    Button saveBtn;
    Spinner citySpinner;

    String token;

    ArrayList<String> cities = new ArrayList<>();
    ArrayList<Integer> cityIds = new ArrayList<>();

    ArrayAdapter<String> adapter;

    Integer selectedPosition;
    Boolean isFromSettings;

    Integer currentCityID;

    Context context;
    OnErrDialogCLickedListener errDialogListener;

    Integer selectedFunction = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
        Integer theme = mSettings.getInt("theme", 0);
        switch (theme) {
            case 0:
                setTheme(R.style.StandartTheme);
                break;
            case 1:
                setTheme(R.style.PatriotTheme);
                break;
            case 2:
                setTheme(R.style.SilverTheme);
                break;
            default:
                break;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_chooser);

        ConstraintLayout flag = findViewById(R.id.flag);

        if (theme == 1) {
            flag.setVisibility(View.VISIBLE);
        } else {
            flag.setVisibility(View.GONE);
        }

        context = this;
        errDialogListener = this;

        token = mSettings.getString("token", "");
        currentCityID = mSettings.getInt("cityId", 1);

        isFromSettings = getIntent().getBooleanExtra("isFromSettings", false);

        saveBtn = findViewById(R.id.saveBtn);

        adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, cities);
        citySpinner = findViewById(R.id.citySpinner);
        citySpinner.setAdapter(adapter);

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                selectedPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }

        });

        saveBtn = findViewById(R.id.saveBtn);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getCities(token);
    }

    private void getCities(String token) {

        selectedFunction = 0;

        RequestBody body = new RequestBody();
        body.token = token;

        NetworkService.getInstance()
                .getJSONApi()
                .getcities(body)
                .enqueue(new Callback<GetCitiesData>() {
                    @Override
                    public void onResponse(@NonNull Call<GetCitiesData> call, @NonNull Response<GetCitiesData> response) {

                        Log.d("Hash", "response: " + response.toString());

                        GetCitiesData responseData = response.body();

                        if (responseData.err_id == 0) {

                            cities.clear();
                            cityIds.clear();

                            for (CityDetail city: responseData.cities) {

                                cities.add(city.name);
                                cityIds.add(city.id);

                            }

                            adapter.notifyDataSetChanged();
                            selectedPosition = cityIds.indexOf(currentCityID);
                            citySpinner.setSelection(selectedPosition);

                        } else {
                            Global.showErrId(context, responseData.err_id, errDialogListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GetCitiesData> call, @NonNull Throwable t) {
                        //textView.append("Error occurred while getting request!");
                        Global.showErrorConnection(context, errDialogListener);
                        t.printStackTrace();
                    }
                });
    }

    public void saveTapped(View v) {

        if (selectedPosition != null) {
            v.setEnabled(false);
            checkPosition();
            v.setEnabled(true);
        } else {
            Global.showErrId(context, 4, errDialogListener);
        }


    }

    private void checkPosition() {

        Integer id = cityIds.get(selectedPosition);
        String cityName = cities.get(selectedPosition);

        setCity(token, id, cityName);

    }


    private void setCity(String token, final Integer id, final String cityName) {

        selectedFunction = 1;

        RequestBody body = new RequestBody();
        body.token = token;
        body.city = id;

        NetworkService.getInstance()
                .getJSONApi()
                .setcity(body)
                .enqueue(new Callback<SetCityData>() {
                    @Override
                    public void onResponse(@NonNull Call<SetCityData> call, @NonNull Response<SetCityData> response) {

                        Log.d("Hash", "response: " + response.toString());

                        SetCityData responseData = response.body();

                        if (responseData.err_id == 0) {

                            SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
                            SharedPreferences.Editor editor = mSettings.edit();
                            editor.putString("cityName", cityName);
                            editor.putInt("cityId", id);
                            editor.putString("dispPhone", responseData.city.dispPhone);
                            editor.commit();

                            if (isFromSettings) {

                                CityChooser.this.finish();

                            } else {

                                Intent intent = new Intent(getApplicationContext(), NewOrder.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                startActivity(intent);

                            }

                        } else {
                            Global.showErrId(context, responseData.err_id, errDialogListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<SetCityData> call, @NonNull Throwable t) {
                        //textView.append("Error occurred while getting request!");
                        Global.showErrorConnection(context, errDialogListener);
                        t.printStackTrace();
                    }
                });

    }

    @Override
    public void onOkButtonCLicked() {

    }

    @Override
    public void onCancelButtonClicked() {

    }

    @Override
    public void onRetryButtonClicked() {

        switch (selectedFunction) {
            case 0:
                getCities(token);
                break;
            case 1:
                checkPosition();
                break;
        }

    }
}
