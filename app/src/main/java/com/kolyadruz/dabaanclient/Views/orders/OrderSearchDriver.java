package com.kolyadruz.dabaanclient.Views.orders;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.kolyadruz.dabaanclient.Global;
import com.kolyadruz.dabaanclient.OnErrDialogCLickedListener;
import com.kolyadruz.dabaanclient.R;
import com.kolyadruz.dabaanclient.Retrofit.NetworkService;
import com.kolyadruz.dabaanclient.Retrofit.RequestBody;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.acceptcancel.AcceptCancelData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.cancelorder.CancelOrderData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.checkorder.CheckOrderData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.updateorder.UpdateOrderData;
import com.kolyadruz.dabaanclient.Views.Settings.UserSettings;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kolyadruz on 31.03.2018.
 */

public class OrderSearchDriver extends AppCompatActivity implements OnErrDialogCLickedListener {

    String token;

    TextView statusTxt;
    TextView detailsTxt;

    Button cancelButton;
    Button updateButton;

    Integer order_id = 0;
    Boolean isCanceled = false;

    Timer taskTimer = new Timer();
    runCheckOrder mRunCheckOrder;

    TextView pointAtxt;
    TextView pointBtxt;
    TextView costTxt;
    TextView commentTxt;

    ImageView imgCost;

    Integer status = 0;
    String driverPhone = "";

    AlertDialog.Builder ad;

    Integer selectedFunction = 0;

    Context context;
    OnErrDialogCLickedListener errDialogListener;

    Integer currentTheme;

    String dispPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
        Integer theme = mSettings.getInt("theme", 0);

        switch (theme) {
            case 0:
                setTheme(R.style.StandartTheme);
                break;
            case 1:
                setTheme(R.style.PatriotTheme);
                break;
            case 2:
                setTheme(R.style.SilverTheme);
                break;
            default:
                break;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordersearchdriver);

        ConstraintLayout flag = findViewById(R.id.flag);

        if (theme == 1) {
            flag.setVisibility(View.VISIBLE);
        } else {
            flag.setVisibility(View.GONE);
        }

        currentTheme = theme;

        order_id = getIntent().getExtras().getInt("order_id", 0);

        statusTxt = findViewById(R.id.statusTxt);
        detailsTxt = findViewById(R.id.detailsTxt);

        cancelButton = findViewById(R.id.cancelButton);
        updateButton = findViewById(R.id.updateButton);
        updateButton.setVisibility(View.GONE);

        pointAtxt = findViewById(R.id.pointAtxt);
        pointBtxt = findViewById(R.id.pointBtxt);
        costTxt = findViewById(R.id.costTxt);
        commentTxt = findViewById(R.id.commentTxt);

        imgCost = findViewById(R.id.imgCost);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
        token = mSettings.getString("token", "");

        dispPhone = mSettings.getString("dispPhone", "");

        Integer theme = mSettings.getInt("theme", 0);

        if (theme != currentTheme) {
            currentTheme = theme;
            finish();
            startActivity(getIntent());
        }

        runCheckOrder();
    }

    public void updateOrder(View view){
        view.setEnabled(false);
        runUpdateOrder();
        view.setEnabled(true);
    }

    public void callTapped(View view) {
        view.setEnabled(false);
        startCallDialog();
        view.setEnabled(true);
    }

    public void startCallDialog() {

        final String[] actions;

        if(driverPhone == "") {
            actions = new String[] {"Диспетчеру"};
        } else {
            actions = new String[] {"Диспетчеру", "Водителю"};
        }

        ad = new AlertDialog.Builder(this);
        ad.setTitle("Позвонить");

        ad.setItems(actions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int item) {
                switch (item) {
                    case 0:
                        Intent intent2 = new Intent(Intent.ACTION_CALL);
                        intent2.setData(Uri.parse("tel: " + dispPhone));
                        if (ContextCompat.checkSelfPermission(OrderSearchDriver.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(OrderSearchDriver.this, new String[]{Manifest.permission.CALL_PHONE},1);
                        } else {
                            OrderSearchDriver.this.startActivity(intent2);
                        }
                        break;
                    case 1:
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel: +" + driverPhone));
                        if (ContextCompat.checkSelfPermission(OrderSearchDriver.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(OrderSearchDriver.this, new String[]{Manifest.permission.CALL_PHONE},1);
                        } else {
                            OrderSearchDriver.this.startActivity(intent);
                        }
                        break;
                    default:
                        break;
                }
            }
        });

        ad.show();

    }

    public void cancelOrder(View view){
        view.setEnabled(false);

        if (isCanceled) {
            runAcceptCancel();
        } else {
            runCancelOrder();
        }

        view.setEnabled(true);
    }

    private void runCheckOrder() {
        checkOrderAC(token, order_id.toString());
    }

    private void runCancelOrder() {
        cancelOrderAC(token, order_id.toString());
    }

    private void runUpdateOrder() {
        updateOrderAC(token, order_id.toString());
    }

    private void runAcceptCancel() {
        acceptCancel(token, order_id.toString());
    }

    @Override
    public void onOkButtonCLicked() {

    }

    @Override
    public void onCancelButtonClicked() {

    }

    @Override
    public void onRetryButtonClicked() {

        switch (selectedFunction) {
            case 0:
                runCheckOrder();
                break;
            case 1:
                runCancelOrder();
                break;
            case 2:
                runUpdateOrder();
                break;
            case 3:
                runAcceptCancel();
                break;
        }

    }

    class runCheckOrder extends TimerTask {
        @Override
        public void run() {
            if(this == null)
                return;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    runCheckOrder();
                }
            });
        }
    }

    public void runCheckOrderWithInterval() {
        if (taskTimer != null) {
            taskTimer.cancel();
        }

        taskTimer = new Timer();
        mRunCheckOrder = new runCheckOrder();
        taskTimer.schedule(mRunCheckOrder, 1000);
    }

    public void settingsTapped(View view){
        view.setEnabled(false);

        Intent intent = new Intent(getApplicationContext(), UserSettings.class);
        startActivity(intent);

        view.setEnabled(true);
    }

    private void checkOrderAC(String token, String order_id) {

        selectedFunction = 0;

        RequestBody body = new RequestBody();
        body.token = token;
        body.order_id = order_id;

        NetworkService.getInstance()
                .getJSONApi()
                .checkorder(body)
                .enqueue(new Callback<CheckOrderData>() {
                    @Override
                    public void onResponse(@NonNull Call<CheckOrderData> call, @NonNull Response<CheckOrderData> response) {

                        Gson gson = new Gson();

                        Log.d("OrderSearchDriver", "response: " + gson.toJson(response.body()));

                        CheckOrderData responseData = response.body();

                        if (responseData.err_id == 0) {

                            status = responseData.order.status;

                            String point_a = responseData.order.point_a;
                            String point_b = responseData.order.point_b;
                            String prim = responseData.order.prim;

                            Integer price = responseData.order.price;
                            Integer discount_sum = responseData.order.discount_sum;

                            if (price > discount_sum) {
                                imgCost.setImageResource(R.drawable.rub_enabled);
                                costTxt.setText(""+price);
                            } else {
                                imgCost.setImageResource(R.drawable.ic_bonus_yellow);
                                costTxt.setText(""+discount_sum + " (бонусами)");
                            }

                            String note = "";

                            if (!prim.equals("")) {
                                note = "Примечание: " + prim;
                            }

                            pointAtxt.setText(point_a);
                            pointBtxt.setText(point_b);
                            commentTxt.setText(prim);

                            if (responseData.order.rem_time >= responseData.order.timeout && responseData.order.timeout > 0) {
                                updateButton.setVisibility(View.VISIBLE) ;
                            } else  {
                                updateButton.setVisibility(View.GONE);
                            }

                            switch (status) {
                                case 1:
                                    statusTxt.setText("Поиск водителя...");

                                    //String detail = "от: " + point_a + " до: " + point_b + " на сумму " + price + " руб. " + note;
                                    //detailsTxt.setText(detail);

                                    detailsTxt.setText("");

                                    runCheckOrderWithInterval();

                                    break;
                                case 2:
                                    statusTxt.setText("Ваш заказ принят");

                                    String name = responseData.driver.name;
                                    String car_model = responseData.driver.car_model;
                                    String car_color = responseData.driver.car_color;
                                    String car_number = responseData.driver.car_number;
                                    Integer arrivalTime = responseData.driver.arrivalTime;

                                    driverPhone = responseData.driver.phone;

                                    String dtail =  "К Вам подъедет " + name + " на " + car_model + " " + car_color + " с гос.номером: " + car_number + " через " + arrivalTime + " мин.";
                                    detailsTxt.setText(dtail);

                                    runCheckOrderWithInterval();
                                    break;
                                case 3:
                                    Intent intent = new Intent(getApplicationContext(), OrderCompleted.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                    startActivity(intent);

                                    break;
                                case 5:
                                    statusTxt.setText("Ваш заказ отменен");

                                    updateButton.setVisibility(View.GONE);
                                    cancelButton.setText("Закрыть");
                                    detailsTxt.setText("Водитель отменил Ваш заказ");
                                    isCanceled = true;
                                    break;
                                case 8:
                                    statusTxt.setText("Ваш заказ отменен");

                                    updateButton.setVisibility(View.GONE);
                                    cancelButton.setText("Закрыть");
                                    detailsTxt.setText("Диспетчер отменил Ваш заказ");
                                    isCanceled = true;
                                    break;
                                case 10:
                                    Intent intnt = new Intent(getApplicationContext(), OrderCompleted.class);
                                    intnt.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intnt.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intnt.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                    startActivity(intnt);
                                    break;
                                default:
                                    break;
                            }
                        } else {
                            Global.showErrId(context, responseData.err_id, errDialogListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<CheckOrderData> call, @NonNull Throwable t) {
                        //textView.append("Error occurred while getting request!");
                        if (context != null) {
                            Global.showErrorConnection(context, errDialogListener);
                            t.printStackTrace();
                        }
                    }
                });
    }

    private void cancelOrderAC(String token, String order_id) {

        selectedFunction = 1;

        RequestBody body = new RequestBody();
        body.token = token;
        body.order_id = order_id;
        body.reason_id = "1";

        NetworkService.getInstance()
                .getJSONApi()
                .cancelorder(body)
                .enqueue(new Callback<CancelOrderData>() {
                    @Override
                    public void onResponse(@NonNull Call<CancelOrderData> call, @NonNull Response<CancelOrderData> response) {

                        Log.d("OrderSearchDriver", "response: " + response.toString());

                        CancelOrderData responseData = response.body();

                        if (responseData.err_id == 0) {

                            Intent intent = new Intent(getApplicationContext(), NewOrder.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);

                        } else {
                            Global.showErrId(context, responseData.err_id, errDialogListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<CancelOrderData> call, @NonNull Throwable t) {
                        //textView.append("Error occurred while getting request!");
                        Global.showErrorConnection(context, errDialogListener);
                        t.printStackTrace();
                    }
                });

    }

    private void updateOrderAC(String token, String order_id) {

        selectedFunction = 2;

        RequestBody body = new RequestBody();
        body.token = token;
        body.order_id = order_id;

        NetworkService.getInstance()
                .getJSONApi()
                .updateorder(body)
                .enqueue(new Callback<UpdateOrderData>() {
                    @Override
                    public void onResponse(@NonNull Call<UpdateOrderData> call, @NonNull Response<UpdateOrderData> response) {

                        Log.d("OrderSearchDriver", "response: " + response.toString());

                        UpdateOrderData responseData = response.body();

                        updateButton.setEnabled(true);

                        if (responseData.err_id == 0) {

                            updateButton.setVisibility(View.VISIBLE);

                        } else {
                            Global.showErrId(context, responseData.err_id, errDialogListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<UpdateOrderData> call, @NonNull Throwable t) {
                        //textView.append("Error occurred while getting request!");
                        Global.showErrorConnection(context, errDialogListener);
                        t.printStackTrace();
                    }
                });

    }

    private void acceptCancel(String token, String order_id) {

        selectedFunction = 3;

        RequestBody body = new RequestBody();
        body.token = token;
        body.order_id = order_id;

        NetworkService.getInstance()
                .getJSONApi()
                .acceptcancel(body)
                .enqueue(new Callback<AcceptCancelData>() {
                    @Override
                    public void onResponse(@NonNull Call<AcceptCancelData> call, @NonNull Response<AcceptCancelData> response) {

                        Log.d("OrderSearchDriver", "response: " + response.toString());

                        AcceptCancelData responseData = response.body();

                        if (responseData.err_id == 0) {
                            Intent intent = new Intent(getApplicationContext(), NewOrder.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<AcceptCancelData> call, @NonNull Throwable t) {
                        //textView.append("Error occurred while getting request!");
                        Global.showErrorConnection(context, errDialogListener);
                        t.printStackTrace();
                    }
                });

    }

}