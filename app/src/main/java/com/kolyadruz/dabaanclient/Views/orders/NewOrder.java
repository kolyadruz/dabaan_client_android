package com.kolyadruz.dabaanclient.Views.orders;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kolyadruz.dabaanclient.Global;
import com.kolyadruz.dabaanclient.OnErrDialogCLickedListener;
import com.kolyadruz.dabaanclient.R;
import com.kolyadruz.dabaanclient.Retrofit.NetworkService;
import com.kolyadruz.dabaanclient.Retrofit.RequestBody;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.addorder.AddOrderData;
import com.kolyadruz.dabaanclient.Views.Settings.UserSettings;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kolyadruz on 31.03.2018.
 */

public class NewOrder extends AppCompatActivity implements OnErrDialogCLickedListener {

    String token;

    EditText pointAtxt;
    EditText pointBtxt;
    EditText costTxt;
    EditText primTxt;

    AppCompatImageView imgPointA;
    AppCompatImageView imgPointB;
    AppCompatImageView imgCost;
    AppCompatImageView imgPrim;

    TextView bonusTxt;
    AppCompatImageButton bonusBtn;

    Integer balance;
    Integer minSumm;

    Button addOrderButton;

    RelativeLayout connectorLine;

    Boolean isPayByBonus = false;

    Context context;
    OnErrDialogCLickedListener errDialogListener;

    Integer currentTheme;

    String dispPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
        Integer theme = mSettings.getInt("theme", 0);
        switch (theme) {
            case 0:
                setTheme(R.style.StandartTheme);
                break;
            case 1:
                setTheme(R.style.PatriotTheme);
                break;
            case 2:
                setTheme(R.style.SilverTheme);
                break;
            default:
                break;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_neworder);

        ConstraintLayout flag = findViewById(R.id.flag);

        if (theme == 1) {
            flag.setVisibility(View.VISIBLE);
        } else {
            flag.setVisibility(View.GONE);
        }

        currentTheme = theme;

        context = this;
        errDialogListener = this;

        pointAtxt = findViewById(R.id.pointAtxt);
        pointBtxt = findViewById(R.id.pointBtxt);
        costTxt = findViewById(R.id.costTxt);
        primTxt = findViewById(R.id.noteTxt);

        bonusTxt = findViewById(R.id.bonusTxt);
        bonusBtn = findViewById(R.id.payByBonusBtn);

        pointAtxt.addTextChangedListener(mWatcher);
        pointBtxt.addTextChangedListener(mWatcher);
        costTxt.addTextChangedListener(mWatcher);
        primTxt.addTextChangedListener(mWatcher);

        imgPointA = findViewById(R.id.imgPointA);
        imgPointB = findViewById(R.id.imgPointB);
        imgCost = findViewById(R.id.imgCost);
        imgPrim = findViewById(R.id.imgPrim);

        connectorLine = findViewById(R.id.connectorLine);

        token = mSettings.getString("token", "");

        balance = mSettings.getInt("balance", 0);
        minSumm = mSettings.getInt("minDiscountSum",0);
        bonusTxt.setText(""+balance);

        pointAtxt.setText(mSettings.getString("pointA", ""));
        pointBtxt.setText(mSettings.getString("pointB", ""));
        costTxt.setText(mSettings.getString("cost", ""));
        primTxt.setText(mSettings.getString("prim", ""));

        addOrderButton = findViewById(R.id.addOrderButton);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
        dispPhone = mSettings.getString("dispPhone", "");
        Integer theme = mSettings.getInt("theme", 0);

        if (theme != currentTheme) {
            currentTheme = theme;
            finish();
            startActivity(getIntent());
        }

    }

    public boolean checkForErrors() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        boolean errors = false;
        String message = "";

        if (pointAtxt.getText().toString().equals("")) {
            errors = true;
            message = "Введите адрес отбытия";
            builder.setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }

        if (pointBtxt.getText().toString().equals("")) {
            errors = true;
            message = "Введите адрес прибытия";
            builder.setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }

        if (costTxt.getText().toString().equals("")) {
            errors = true;
            message = "Введите цену";
            builder.setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }

        return errors;
    }

    public void showBonusInfo(View view) {
        view.setEnabled(false);
        Intent intent = new Intent(getApplicationContext(), BonusInfo.class);
        startActivity(intent);
        view.setEnabled(true);
    }

    public void addOrder(View view){
        view.setEnabled(false);

        if (!checkForErrors()) {
            runSendOrder();
        }

        view.setEnabled(true);
    }

    public void settingsTapped(View view){
        view.setEnabled(false);

        Intent intent = new Intent(getApplicationContext(), UserSettings.class);

        startActivity(intent);

        view.setEnabled(true);

    }

    public void payByBonus(View view) {

        view.setEnabled(false);

        if (!isPayByBonus && !costTxt.getText().toString().equals("")) {

            String cost = costTxt.getText().toString();
            Integer costInt = Integer.parseInt(cost);

            if (costInt >= minSumm) {

                if (balance >= costInt) {

                    isPayByBonus = true;
                    bonusBtn.setColorFilter(fetchAccentColor());

                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("У Вас недостаточно бонусных баллов")
                            .setCancelable(false)
                            .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

            } else {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Сумма проезда должна быть не менее " + minSumm + " руб. для оплаты проезда бонусами.")
                        .setCancelable(false)
                        .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }

        } else {

            isPayByBonus = false;
            bonusBtn.setColorFilter(ContextCompat.getColor(this, R.color.colorLightGray));
        }

        view.setEnabled(true);

    }

    public void callTaxi(View view) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel: " + dispPhone));
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},1);
        } else {
            this.startActivity(intent);
        }
    }


    private void runSendOrder() {

        String pointA = pointAtxt.getText().toString();
        String pointB = pointBtxt.getText().toString();
        String cost = costTxt.getText().toString();
        String prim = primTxt.getText().toString();

        sendOrder(token, pointA, pointB, cost, prim);
    }

    private void sendOrder(String token, String pointA, String pointB, String cost, String prim) {

        RequestBody body = new RequestBody();

        body.token = token;
        body.point_a = pointA;
        body.point_b = pointB;
        body.price = cost;
        body.prim = prim;
        body.payment_id = "0";
        if (isPayByBonus) {
            body.discount_sum = cost;
        } else {
           body.discount_sum = "0";
        }

        NetworkService.getInstance()
                .getJSONApi()
                .addorder(body)
                .enqueue(new Callback<AddOrderData>() {
                    @Override
                    public void onResponse(@NonNull Call<AddOrderData> call, @NonNull Response<AddOrderData> response) {

                        Log.d("Hash", "response: " + response.toString());

                        AddOrderData responseData = response.body();

                        if (responseData.err_id == 0) {

                            Integer order_id = responseData.order_id;
                            Log.d("NewOrder:","Заказ успешно добавлен");

                            SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
                            SharedPreferences.Editor editor = mSettings.edit();
                            editor.putString("pointA", pointAtxt.getText().toString());
                            editor.putString("pointB", pointBtxt.getText().toString());
                            editor.putString("cost", costTxt.getText().toString());
                            editor.putString("prim", primTxt.getText().toString());
                            editor.commit();

                            Log.d("NewOrder:","ID добавленного заказа " + order_id);
                            Log.d("NewOrder:","Переход в OrderSearchDriver.java");

                            Intent intent = new Intent(getApplicationContext(), OrderSearchDriver.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            intent.putExtra("order_id", order_id);

                            startActivity(intent);
                        } else {
                            Global.showErrId(context, responseData.err_id, errDialogListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<AddOrderData> call, @NonNull Throwable t) {
                        Global.showErrorConnection(context, errDialogListener);
                        t.printStackTrace();
                    }
                });
    }

    private final TextWatcher mWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkValues();
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private void checkValues() {
        if (pointAtxt.length() > 0 && pointBtxt.length() > 0) {
            connectorLine.setBackgroundColor(fetchAccentColor());
        } else {
            connectorLine.setBackgroundColor(ContextCompat.getColor(this, R.color.colorLightGray));
        }

        if (pointAtxt.length() > 0) {
            imgPointA.setColorFilter(fetchAccentColor());
        } else {
            imgPointA.setColorFilter(ContextCompat.getColor(this, R.color.colorLightGray));
        }

        if (pointBtxt.length() > 0) {
            imgPointB.setColorFilter(fetchAccentColor());
        } else {
            imgPointB.setColorFilter(ContextCompat.getColor(this, R.color.colorLightGray));
        }

        if (costTxt.length() > 0) {
            imgCost.setColorFilter(fetchAccentColor());
        } else {
            imgCost.setColorFilter(ContextCompat.getColor(this, R.color.colorLightGray));
        }

        if (primTxt.length() > 0) {
            imgPrim.setColorFilter(fetchAccentColor());
        } else {
            imgPrim.setColorFilter(ContextCompat.getColor(this, R.color.colorLightGray));
        }

    }

    private int fetchAccentColor() {
        TypedValue typedValue = new TypedValue();

        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorAccent });
        int color = a.getColor(0, 0);

        a.recycle();

        return color;
    }

    @Override
    public void onOkButtonCLicked() {

    }

    @Override
    public void onCancelButtonClicked() {

    }

    @Override
    public void onRetryButtonClicked() {
        runSendOrder();
    }
}