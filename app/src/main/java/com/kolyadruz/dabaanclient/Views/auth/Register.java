package com.kolyadruz.dabaanclient.Views.auth;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kolyadruz.dabaanclient.Global;
import com.kolyadruz.dabaanclient.OnErrDialogCLickedListener;
import com.kolyadruz.dabaanclient.R;
import com.kolyadruz.dabaanclient.Retrofit.NetworkService;
import com.kolyadruz.dabaanclient.Retrofit.RequestBody;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.reg.RegData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kolyadruz on 31.03.2018.
 */

public class Register extends AppCompatActivity implements OnErrDialogCLickedListener {

    EditText phoneField;
    EditText nameField;

    Button sendSMSbutton;

    Context context;
    OnErrDialogCLickedListener errDialogListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        context = this;
        errDialogListener = this;

        nameField = findViewById(R.id.nameField);
        phoneField = findViewById(R.id.phoneField);

        sendSMSbutton = findViewById(R.id.sendSMSbutton);
    }

    public void sendMessage(View view){
        view.setEnabled(false);
        checkFields();
        view.setEnabled(true);
    }

    private void checkFields() {

        if (phoneField.length() == 10) {
            if (!nameField.equals("")) {
                runTakeCode(phoneField.getText().toString(), nameField.getText().toString());
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                String message = "Введите Ваше имя";
                builder.setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        } else {

            Log.d("Reg", "Error at checking");

            Global.showErrId(context, 1, errDialogListener);
        }

    }

    private void runTakeCode(String phone, String name) {

        RequestBody body = new RequestBody();
        body.phone = phone;
        body.name = name;

        Log.d("Reg", "phone: " + phone + " name: " + name);

        NetworkService.getInstance()
                .getJSONApi()
                .reg(body)
                .enqueue(new Callback<RegData>() {
                    @Override
                    public void onResponse(@NonNull Call<RegData> call, @NonNull Response<RegData> response) {

                        Log.d("Reg", "response err_id: " + response.body().err_id);

                        RegData responseData = response.body();

                        if (responseData.err_id == 0) {

                            String phone = phoneField.getText().toString();

                            SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
                            SharedPreferences.Editor editor = mSettings.edit();
                            editor.putString("phone", phone);
                            editor.commit();

                            Log.d("Register:","Переход в Hash.java");

                            Intent intent = new Intent(getApplicationContext(), Hash.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);
                        } else {
                            Global.showErrId(context, responseData.err_id, errDialogListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<RegData> call, @NonNull Throwable t) {
                        //textView.append("Error occurred while getting request!");
                        Global.showErrorConnection(context, errDialogListener);
                        t.printStackTrace();
                    }
                });

    }

    @Override
    public void onOkButtonCLicked() {

    }

    @Override
    public void onCancelButtonClicked() {

    }

    @Override
    public void onRetryButtonClicked() {
        checkFields();
    }
}