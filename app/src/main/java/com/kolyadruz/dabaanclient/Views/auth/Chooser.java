package com.kolyadruz.dabaanclient.Views.auth;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.kolyadruz.dabaanclient.ChooserPagerAdapter;
import com.kolyadruz.dabaanclient.R;

import java.util.ArrayList;
import java.util.List;

public class Chooser extends AppCompatActivity {

    LayoutInflater layoutInflater;

    ViewPager pager;
    ChooserPagerAdapter pagerAdapter;
    List<View> pages = new ArrayList<View>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chooser);

        layoutInflater = getLayoutInflater();

        pager = findViewById(R.id.viewPager);
        pagerAdapter = new ChooserPagerAdapter(pages);

        pager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabDots);
        tabLayout.setupWithViewPager(pager, true);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        pages.clear();

        View page1 = layoutInflater.inflate(R.layout.chooser_first_page, null);
        pages.add(page1);

        View page2 = layoutInflater.inflate(R.layout.chooser_second_page, null);
        pages.add(page2);

        View page3 = layoutInflater.inflate(R.layout.chooser_third_page, null);
        pages.add(page3);

        pagerAdapter.notifyDataSetChanged();

    }

    public void goToLogin(View v) {

        Intent intent = new Intent(getApplicationContext(), Register.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        startActivity(intent);

    }

}

