package com.kolyadruz.dabaanclient.Views.Settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.kolyadruz.dabaanclient.Global;
import com.kolyadruz.dabaanclient.OnErrDialogCLickedListener;
import com.kolyadruz.dabaanclient.R;
import com.kolyadruz.dabaanclient.Retrofit.NetworkService;
import com.kolyadruz.dabaanclient.Retrofit.RequestBody;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.chuserinfo.ChUserInfoData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.loaduserinfo.LoadUserInfoData;
import com.kolyadruz.dabaanclient.Views.auth.Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kolyadruz on 31.03.2018.
 */

public class UserSettings extends AppCompatActivity implements OnErrDialogCLickedListener {

    AlertDialog.Builder ad;
    String token;
    EditText nameField;
    ImageButton saveButton;

    Integer cityId;
    String cityName;

    Button changeCityBtn;

    Integer selectedFunction = 0;

    Context context;
    OnErrDialogCLickedListener errDialogListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
        Integer theme = mSettings.getInt("theme", 0);
        switch (theme) {
            case 0:
                setTheme(R.style.StandartTheme);
                break;
            case 1:
                setTheme(R.style.PatriotTheme);
                break;
            case 2:
                setTheme(R.style.SilverTheme);
                break;
            default:
                break;
        }

        setContentView(R.layout.activity_usersettings);

        ConstraintLayout flag = findViewById(R.id.flag);

        if (theme == 1) {
            flag.setVisibility(View.VISIBLE);
        } else {
            flag.setVisibility(View.GONE);
        }

        context = this;
        errDialogListener = this;

        nameField = findViewById(R.id.nameField);
        saveButton = findViewById(R.id.saveButton);
        changeCityBtn = findViewById(R.id.button3);
    }

    @Override
    protected void onResume() {
        super.onResume();
        runLoadUserInfo();

        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
        cityId = mSettings.getInt("cityId", 1);
        cityName = mSettings.getString("cityName", "");

        changeCityBtn.setText(cityName);
    }

    public void saveTapped(View view){
        view.setEnabled(false);

        if (nameField.length() > 0) {
            runSaveUserInfo();
        } else {
            Global.showErrId(context, 4, errDialogListener);
        }

        view.setEnabled(true);
    }

    public void cityTapped(View view) {
        Intent intent = new Intent(getApplicationContext(), CityChooser.class);
        intent.putExtra("isFromSettings", true);

        startActivity(intent);
    }

    public void changeThemeTapped(View view){
        startDialog();
    }

    public void startDialog(){
        final String[] actions ={"Стандартная", "Патриотичная", "Светлая", "Отмена"};
        ad = new AlertDialog.Builder(this);
        ad.setTitle("Выберите тему");

        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
        final SharedPreferences.Editor editor = mSettings.edit();

        ad.setItems(actions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 3:
                        break;
                    default:
                        editor.putInt("theme", item);
                        editor.commit();

                        finish();
                        startActivity(getIntent());

                        break;
                }
            }
        });



        ad.show();
    }

    public void exitTapped(View view){
        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("token", "");
        editor.commit();

        Intent intent = new Intent(getApplicationContext(), Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        startActivity(intent);
    }

    private void runLoadUserInfo() {
        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
        token = mSettings.getString("token", "");

        loadUserInfo(token);
    }

    private void runSaveUserInfo() {
        SharedPreferences mSettings = getSharedPreferences(Global.prefsName, MODE_PRIVATE);
        token = mSettings.getString("token", "");
        String name = nameField.getText().toString();

        saveUserInfo(token, name);
    }

    private void loadUserInfo(String token) {

        selectedFunction = 0;

        RequestBody body = new RequestBody();
        body.token = token;

        NetworkService.getInstance()
                .getJSONApi()
                .loaduserinfo(body)
                .enqueue(new Callback<LoadUserInfoData>() {
                    @Override
                    public void onResponse(@NonNull Call<LoadUserInfoData> call, @NonNull Response<LoadUserInfoData> response) {

                        Log.d("Hash", "response: " + response.toString());

                        LoadUserInfoData responseData = response.body();

                        if (responseData.err_id == 0) {

                            nameField.setText(responseData.name);

                        } else {
                            Global.showErrId(context, responseData.err_id, errDialogListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<LoadUserInfoData> call, @NonNull Throwable t) {
                        //textView.append("Error occurred while getting request!");
                        Global.showErrorConnection(context, errDialogListener);
                        t.printStackTrace();
                    }
                });
    }

    private void saveUserInfo(String token, String name) {

        selectedFunction = 1;

        RequestBody body = new RequestBody();
        body.token = token;
        body.name = name;

        NetworkService.getInstance()
                .getJSONApi()
                .chuserinfo(body)
                .enqueue(new Callback<ChUserInfoData>() {
                    @Override
                    public void onResponse(@NonNull Call<ChUserInfoData> call, @NonNull Response<ChUserInfoData> response) {

                        Log.d("ChUserInfo", "response: " + response.toString());

                        ChUserInfoData responseData = response.body();

                        if (responseData.err_id == 0) {

                            finish();

                        } else {
                            Global.showErrId(context, responseData.err_id, errDialogListener);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ChUserInfoData> call, @NonNull Throwable t) {
                        //textView.append("Error occurred while getting request!");
                        Global.showErrorConnection(context, errDialogListener);
                        t.printStackTrace();
                    }
                });

    }

    @Override
    public void onOkButtonCLicked() {

    }

    @Override
    public void onCancelButtonClicked() {

    }

    @Override
    public void onRetryButtonClicked() {
        switch (selectedFunction) {
            case 0:
                runLoadUserInfo();
                break;
            case 1:
                runSaveUserInfo();
                break;
        }
    }
}
