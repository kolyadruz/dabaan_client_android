package com.kolyadruz.dabaanclient;

public interface OnErrDialogCLickedListener {

    void onOkButtonCLicked();

    void onCancelButtonClicked();

    void onRetryButtonClicked();

}
