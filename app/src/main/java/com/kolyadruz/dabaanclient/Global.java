package com.kolyadruz.dabaanclient;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public final class Global {

    public static final String prefsName = "DABAAN_CLIENT";

    public static final String api_url = "http://dabaan.rvcode.ru/mobile/";

    public static void showErrId(Context context, Integer err_id, final OnErrDialogCLickedListener listener) {

        if (context != null) {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            String message = "";

            if (err_id == 1) {
                message = "Указан неправильный номер телефона";
            } else if (err_id == 2) {
                message = "Повторите попытку позднее";
            } else if (err_id == 3) {
                message = "Ошибка работы токена";
            } else if (err_id == 4) {
                message = "Заполните все поля";
            } else if (err_id == 5) {
                message = "Ваш аккаунт неактивен";
            } else if (err_id == 6) {
                message = "Пользователь отсутствует или заблокирован";
            } else if (err_id == 7) {
                message = "Заказ закрыт, или отменен";
            } else if (err_id == 8) {
                message = "Недостаточно средств на балансе";
            } else if (err_id == 9) {
                message = "Заказ уже принят";
            } else if (err_id == 10) {
                message = "Ошибка интернет соединения";
            }

            builder.setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            listener.onOkButtonCLicked();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }

    public static void showErrorConnection (Context context, final OnErrDialogCLickedListener listener) {

        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Остуствует интернет подключение")
                    .setCancelable(false)
                    .setPositiveButton("ПОВТОРИТЬ", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            listener.onRetryButtonClicked();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

}