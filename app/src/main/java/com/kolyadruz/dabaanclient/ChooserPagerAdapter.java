package com.kolyadruz.dabaanclient;

import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class ChooserPagerAdapter extends PagerAdapter {

    List<View> pages = null;

    public ChooserPagerAdapter(List<View> pages){
        this.pages = pages;
    }

    @Override
    public Object instantiateItem(ViewGroup viewGroup, int position){
        View v = pages.get(position);
        viewGroup.addView(v, 0);
        return v;
    }

    @Override
    public int getCount(){
        return pages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object){
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable arg0, ClassLoader arg1){
    }

    @Override
    public Parcelable saveState(){
        return null;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup viewGroup, int position, Object object) {
        ((ViewPager) viewGroup).removeView((View) object);
    }
}