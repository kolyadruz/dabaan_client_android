package com.kolyadruz.dabaanclient.Retrofit.Interface;

import com.kolyadruz.dabaanclient.Retrofit.RequestBody;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.acceptcancel.AcceptCancelData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.addorder.AddOrderData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.cancelorder.CancelOrderData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.checkhash.CheckHashData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.checkorder.CheckOrderData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.chuserinfo.ChUserInfoData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.getcities.GetCitiesData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.loaduserinfo.LoadUserInfoData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.reg.RegData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.checklogin.ClientLoginData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.setcity.SetCityData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.updateorder.UpdateOrderData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface JSONPlaceHolderApi {

    @POST("clientlogin")
    public Call<ClientLoginData> clientlogin(@Body RequestBody requestBody);

    @POST("reg")
    public Call<RegData> reg(@Body RequestBody requestBody);

    @POST("checkhash")
    public Call<CheckHashData> checkhash(@Body RequestBody requestBody);

    @POST("addorder")
    public Call<AddOrderData> addorder(@Body RequestBody requestBody);

    @POST("checkorder")
    public Call<CheckOrderData> checkorder(@Body RequestBody requestBody);

    @POST("cancelorder")
    public Call<CancelOrderData> cancelorder(@Body RequestBody requestBody);

    @POST("updateorder")
    public Call<UpdateOrderData> updateorder(@Body RequestBody requestBody);

    @POST("getcities")
    public Call<GetCitiesData> getcities(@Body RequestBody requestBody);

    @POST("setcity")
    public Call<SetCityData> setcity(@Body RequestBody requestBody);

    @POST("load_userinfo")
    public Call<LoadUserInfoData> loaduserinfo(@Body RequestBody requestBody);

    @POST("ch_userinfo")
    public Call<ChUserInfoData> chuserinfo(@Body RequestBody requestBody);

    @POST("acceptcancel")
    public Call<AcceptCancelData> acceptcancel(@Body RequestBody requestBody);

}
