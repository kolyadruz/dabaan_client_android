package com.kolyadruz.dabaanclient.Retrofit;

public class RequestBody {

    public String token;
    public String fb_token;
    public String phone;
    public String name;
    public String hash;
    public String point_a;
    public String point_b;
    public String prim;
    public String price;
    public String payment_id;
    public String discount_sum;
    public String order_id;
    public String reason_id;
    public Integer city;

}
