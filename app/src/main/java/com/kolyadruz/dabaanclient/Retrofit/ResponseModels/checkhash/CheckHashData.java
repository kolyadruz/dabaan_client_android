package com.kolyadruz.dabaanclient.Retrofit.ResponseModels.checkhash;

import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.checkorder.OrderData;
import com.kolyadruz.dabaanclient.Retrofit.ResponseModels.getcities.CityDetail;

public class CheckHashData {

    public Integer err_id;
    public String token;
    public Integer min_discount_sum;
    public Integer balance;

    public OrderData order;

    public CityDetail city;

}
