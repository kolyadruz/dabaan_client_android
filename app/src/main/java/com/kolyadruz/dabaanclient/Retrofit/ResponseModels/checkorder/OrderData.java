package com.kolyadruz.dabaanclient.Retrofit.ResponseModels.checkorder;

public class OrderData {

    public Integer status;

    public Integer order_id;

    public String point_a;
    public String point_b;

    public Integer discount_sum;
    public Integer start_price;
    public Integer price;

    public String prim;
    public Integer payment_id;

    public Integer rem_time;
    public Integer timeout;

}
